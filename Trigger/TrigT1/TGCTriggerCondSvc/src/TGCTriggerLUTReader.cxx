/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "TGCTriggerCondSvc/TGCTriggerLUTReader.h"
#include "TGCTriggerCondSvc/TGCTriggerLUTs.h"


TGCTriggerLUTReader::TGCTriggerLUTReader(int lutType):
  m_lutType(lutType)
{
}
