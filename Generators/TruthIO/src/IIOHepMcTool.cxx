/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/////////////////////////////////////////////////////////////////// 
// IIOHepMcTool.cxx 
// Implementation file for class IIOHepMcTool
// Author: S.Binet<binet@cern.ch>
/////////////////////////////////////////////////////////////////// 

// Framework includes

// McParticleKernel includes
#include "TruthIO/IIOHepMcTool.h"

/////////////////////////////////////////////////////////////////// 
// Public methods: 
/////////////////////////////////////////////////////////////////// 

/// Destructor
///////////////
IIOHepMcTool::~IIOHepMcTool()
{}
