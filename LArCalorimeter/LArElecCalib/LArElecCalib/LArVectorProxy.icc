/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file LArElecCalib/LArVectorProxy.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2011
 * @brief Proxy for accessing a range of float values like a vector.
 */


#include <stdexcept>


/**
 * @brief Default constructor. 
 *        Creates the proxy in an invalid state.
 */
inline
LArVectorProxy::LArVectorProxy()
  : std::span<const float>() {}


/**
 * @brief Construct a proxy referencing an existing vector.
 * @param vec The existing vector to reference.
 */
inline
LArVectorProxy::LArVectorProxy (const std::vector<value_type>& vec)
  :  std::span<const float>(vec.cbegin(),vec.cend())
{
}


/**
 * @brief Construct a proxy referencing a range of vectors in memory.
 * @param beg Pointer to the start of the range.
 * @param end Pointer to the (exclusive) end of the range.
   */
inline
LArVectorProxy::LArVectorProxy (const value_type* beg, const value_type* end)
  :  std::span<const float>(beg,end)
{
}


/**
 * @brief Test to see if the proxy has been initialized.
 */
inline
bool LArVectorProxy::valid() const
{
  return data() != nullptr;
}


/**
 * @brief Vector indexing with bounds check.
 */
inline
LArVectorProxy::value_type LArVectorProxy::at (size_t i) const
{
  if (i >= size())
    throw std::out_of_range ("LArVectorProxy::at");
  return operator[](i);
}


/**
 * @brief Convert back to a vector.
 */
inline
std::vector<LArVectorProxy::value_type> LArVectorProxy::asVector() const
{
  return std::vector<value_type> (begin(), end());
}
